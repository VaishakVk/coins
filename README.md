## Coins API

### Introduction

This project contains two elements

- Scheduler - Runs at every `x` seconds based on the number of coins/currencies to get the latest prices and store it in DB.
- API - Publically exposed endpoints to get the price details. First calls external API and if down takes data from DB.

### Steps to run

- Clone the repository to the local device
- Create a `.env` file and store the secrets. Example secrets are found in `.env.example`

```
Note - if you are using local mongoDB - use MONGO_URL as mongodb://host.docker.internal:27017/<dbName>
```

- Make sure `docker` and `docker-compose` is installed on the system.
- Run `docker-compose up`

### API endoints

- `/coin`
  Sample CURL request

```
curl --location --request GET 'http://localhost:5001/coin?coins=BTC,ETH&currencies=USD,INR'
```

#### Query Parameters

- coins(required) - list of coins whose price is required(comma separated)
- currencies(required) - list of currencies whose price is required(comma separated)

### Scheduling

Schedule time is based on the number of coins and currencies combinations. If the number of coins/currecny increases then schedule time of cron job is reduced.

### Optimization

- Currently, all the coins are selected and inserted to DB everytime. Introduce Cache to ensure that only the coins that are used the most can be on service. Rest can be adhoc.

### Scalablity

- Currently, it is a monolith. Create separate microservices for both scheduler and API so that any instance that requires more load can be scaled separately and can be loosely coupled.
