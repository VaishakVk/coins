export const CoinsExternalAPIBaseURL = 'https://min-api.cryptocompare.com/';
export const Apikey = process.env.API_KEY;
export const RefreshRate = 2 * 60;
export const CurrenciesLimit = 25;
export const CoinsLimit = 200;
