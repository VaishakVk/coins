export type Batch = {
  coins: string[];
  currencies: string[];
};
export type GenerateBatchesType = {
  batches: Batch[];
  cronDuration: number;
};
