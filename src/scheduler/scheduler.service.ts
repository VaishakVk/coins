import { getAllCoins, getPriceOfCoins } from '../coin/coin.service';
import { CoinsLimit, CurrenciesLimit, RefreshRate } from '../constants';
import { CronJob } from 'cron';
import currencyCodes from 'currency-codes';
import { Batch, GenerateBatchesType } from './scheduler.types';
import { BulkWrite, DeleteMany } from '../db/coinDetails/coinDetails.dao';

const generateBatchesAndRefreshRate =
  async (): Promise<GenerateBatchesType> => {
    const batches: Batch[] = [];

    // Get currencies and coins supported
    const currencies = getAllCurrencyCodes();
    const coins = await getAllCoins();

    // Calculate number of batches
    const currencyBatchesNumber = Math.ceil(
      currencies.length / CurrenciesLimit,
    );
    const coinBatchesNumber = Math.ceil(coins.length / CoinsLimit);
    const totalCombinations = currencyBatchesNumber * coinBatchesNumber;

    // Calculate the cron job gap between two batches
    const cronDuration = Math.ceil(RefreshRate / totalCombinations);

    // Generate batches
    const coinBatches = [];
    const currencyBatches = [];

    for (let i = 0; i < currencyBatchesNumber; i++) {
      const batch = currencies.slice(
        i * CurrenciesLimit,
        Math.min((i + 1) * CurrenciesLimit, currencies.length),
      );
      currencyBatches.push(batch);
    }

    for (let i = 0; i < coinBatchesNumber; i++) {
      const batch = coins.slice(
        i * CoinsLimit,
        Math.min((i + 1) * CoinsLimit, coins.length),
      );
      coinBatches.push(batch);
    }

    for (const curr of currencyBatches) {
      for (const coin of coinBatches) {
        batches.push({ coins: coin, currencies: curr });
      }
    }

    return { cronDuration, batches };
  };

const getAllCurrencyCodes = (): string[] => {
  return currencyCodes.codes();
};

const schedule = async () => {
  const { cronDuration, batches } = await generateBatchesAndRefreshRate();
  let batchNumber = 0;
  const job = new CronJob(`*/${cronDuration} * * * * *`, async () => {
    try {
      batchNumber = (batchNumber + 1) % batches.length;
      console.log('Starting Batch - ', batchNumber);
      const batch = batches[batchNumber];
      const dbPayload = await getPriceOfCoins(batch.coins, batch.currencies);

      // Delete old data
      const deleteQuery = {
        currency: { $in: batch.currencies },
        symbol: { $in: batch.coins },
      };
      await DeleteMany(deleteQuery);

      // Insert new records
      await BulkWrite(dbPayload);
    } catch (err) {
      console.error(err);
    }
  });
  job.start();
};

export default schedule;
