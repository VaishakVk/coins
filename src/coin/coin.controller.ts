import { Request, Response } from 'express';
import { validationResult } from 'express-validator';
import { StatusCodes } from 'http-status-codes';
import { errorResponse, successResponse } from '../api/formatter';
import ICoinDetails from '../db/coinDetails/coinDetails.interface';
import { getPriceOfCoins, getPriceOfCoinsFromDb } from './coin.service';

/**
 * @description Get coins data
 * @param req
 * @param res
 * @returns
 */
const getCoins = async (req: Request, res: Response) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return errorResponse(res, StatusCodes.BAD_REQUEST, errors.array());
    }
    const coins = req.query.coins as string;
    const currencies = req.query.currencies as string;

    // Run External API to get data - if it fails take result from DB
    let result: ICoinDetails[];
    try {
      result = await getPriceOfCoins(coins.split(','), currencies.split(','));
    } catch (err) {
      result = await getPriceOfCoinsFromDb(
        coins.split(','),
        currencies.split(','),
      );
    }
    return successResponse(res, StatusCodes.OK, result);
  } catch (err) {
    console.log(err);
    return errorResponse(res, StatusCodes.INTERNAL_SERVER_ERROR, err);
  }
};

export { getCoins };
