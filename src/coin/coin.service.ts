import axios from 'axios';
import { CoinsExternalAPIBaseURL, Apikey } from '../constants';
import urlJoin from 'url-join';
import { CoinListResponse, GetCoinPriceResponse } from './coin.types';
import { FindMany } from '../db/coinDetails/coinDetails.dao';
import ICoinDetails from '../db/coinDetails/coinDetails.interface';
import { formatCoin } from './coin.utils';

/**
 * Handler to get all the coins from cryptocompare
 */
const getAllCoins = async (): Promise<string[]> => {
  const apiUrl = urlJoin(
    CoinsExternalAPIBaseURL,
    'data/blockchain/list',
    `?api_key=${Apikey}&summary=true`,
  );
  const res = await axios.get<CoinListResponse>(apiUrl);
  if (res.data.Response == 'Error') {
    console.error('Error getting coins');
    process.exit(1);
  }
  return Object.keys(res.data.Data);
};

/**
 * Handler to get price of the coins from cryptocompare for each currency
 */
const getPriceOfCoins = async (
  coins: string[],
  currencies: string[],
): Promise<ICoinDetails[]> => {
  const apiUrl = urlJoin(
    CoinsExternalAPIBaseURL,
    'data/pricemultifull',
    `?api_key=${Apikey}&fsyms=${coins.join(',')}&tsyms=${currencies.join(',')}`,
  );
  const res = await axios.get<GetCoinPriceResponse>(apiUrl);
  if (res.data.Response == 'Error') {
    throw 'Error contacting API';
  } else {
    return formatCoin(res.data);
  }
};

/**
 * Handler to get price of the coins from DB for each currency
 */
const getPriceOfCoinsFromDb = async (
  coins: string[],
  currencies: string[],
): Promise<ICoinDetails[]> => {
  const query = { name: { $in: coins }, currency: { $in: currencies } };
  return FindMany(query);
};

export { getAllCoins, getPriceOfCoins, getPriceOfCoinsFromDb };
