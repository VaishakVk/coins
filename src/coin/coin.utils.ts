import ICoinDetails from '../db/coinDetails/coinDetails.interface';
import { GetCoinPriceResponse } from './coin.types';

const formatCoin = (prices: GetCoinPriceResponse): ICoinDetails[] => {
  const rawPrices = prices.RAW;
  const coins = Object.keys(rawPrices);
  const response: ICoinDetails[] = [];
  for (const coin of coins) {
    const currencies = Object.keys(rawPrices[coin]);
    for (const currency of currencies) {
      const coinsDataModel: ICoinDetails = {
        name: coin,
        currency,
        market: rawPrices[coin][currency].MARKET,
        symbol: rawPrices[coin][currency].FROMSYMBOL,
        price: rawPrices[coin][currency].PRICE,
        lastUpdate: new Date(rawPrices[coin][currency].LASTUPDATE),
        volumeDay: rawPrices[coin][currency].VOLUMEDAY,
        openDay: rawPrices[coin][currency].OPENDAY,
        highDay: rawPrices[coin][currency].HIGHDAY,
        lowDay: rawPrices[coin][currency].LOWDAY,
        changeDay: rawPrices[coin][currency].CHANGEDAY,
        changePercentage: rawPrices[coin][currency].CHANGEPCTDAY,
        imageUrl: rawPrices[coin][currency].IMAGEURL,
      };
      response.push(coinsDataModel);
    }
  }

  return response;
};

export { formatCoin };
