import { config } from 'dotenv';
config();

import apiHandler from './api/app';
import connectToDb from './db';
import schedule from './scheduler/scheduler.service';

const start = async () => {
  await connectToDb();
  await schedule();
  apiHandler();
};

start();
