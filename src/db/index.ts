import { connect } from 'mongoose';

const connectToDb = () => {
  return new Promise((resolve) => {
    const MongoUrl = process.env.MONGO_URL;
    if (!MongoUrl) {
      console.log(`Mongo URL not found`);
      process.exit(1);
    }
    connect(MongoUrl, {}, (err) => {
      if (err) {
        console.log(`Error connecting to Database: ${err}`);
        process.exit(1);
      } else {
        console.log('Connected to Database...');
        resolve(true);
      }
    });
  });
};

export default connectToDb;
