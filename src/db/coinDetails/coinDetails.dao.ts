import { FilterQuery } from 'mongoose';
import ICoinDetails from './coinDetails.interface';
import CoinDetails from './coinDetails.model';

const FindOne = async (query: any) => {
  return CoinDetails.findOne(query);
};

const FindMany = async (query: FilterQuery<ICoinDetails>) => {
  return CoinDetails.find(query);
};

const DeleteMany = async (query: FilterQuery<ICoinDetails>) => {
  return CoinDetails.deleteMany(query);
};

const Save = async (query: ICoinDetails) => {
  const coinData = new CoinDetails(query);
  return coinData.save();
};

const BulkWrite = async (payload: ICoinDetails[]) => {
  return CoinDetails.collection.insertMany(payload);
};

export { Save, FindOne, FindMany, DeleteMany, BulkWrite };
