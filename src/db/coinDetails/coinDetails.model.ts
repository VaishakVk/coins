import { model, Model, Schema } from 'mongoose';
import ICoinDetails from './coinDetails.interface';

const CoinDetailsSchema: Schema = new Schema(
  {
    name: { type: String, required: true },
    market: { type: String, required: true },
    symbol: { type: String, required: true },
    currency: { type: String, required: true },
    price: { type: Number, required: true },
    lastUpdate: { type: Date, required: true },
    volumeDay: { type: Number, required: true },
    openDay: { type: Number, required: true },
    highDay: { type: Number, required: true },
    lowDay: { type: Number, required: true },
    changeDay: { type: Number, required: true },
    changePercentage: { type: Number, required: true },
    imageUrl: { type: String, required: true },
  },
  { timestamps: true },
);

const CoinDetails: Model<ICoinDetails> = model(
  'CoinDetails',
  CoinDetailsSchema,
);

export default CoinDetails;
