export default interface ICoinDetails {
  name: string;
  market: string;
  symbol: string;
  currency: string;
  price: number;
  lastUpdate: Date;
  volumeDay: number;
  openDay: number;
  highDay: number;
  lowDay: number;
  changeDay: number;
  changePercentage: number;
  imageUrl: string;
}
