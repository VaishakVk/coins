import { Response } from 'express';
import { StatusCodes } from 'http-status-codes';
/**
 * @description Handler to format success response
 * @param {Express.Response} res
 * @param {number} statusCode
 * @param {any} data
 */
const successResponse = (res: Response, statusCode: number, data: any) => {
  res.status(statusCode).send({
    success: true,
    code: statusCode,
    message: data,
  });
};

/**
 * @description Handler to format error response
 * @param {Express.Response} res
 * @param {number} statusCode
 * @param {any} error
 */
const errorResponse = (res: Response, statusCode: number, err: any) => {
  res.status(statusCode || StatusCodes.INTERNAL_SERVER_ERROR).send({
    success: false,
    code: statusCode || StatusCodes.INTERNAL_SERVER_ERROR,
    error: err.message || err,
  });
};

export { successResponse, errorResponse };
