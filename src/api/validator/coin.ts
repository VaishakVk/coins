import { query } from 'express-validator';

const getCoinsValidator = () => {
  return [
    query('coins').isString().withMessage('coins should be a string'),
    query('currencies').isString().withMessage('currencies should string'),
  ];
};

export { getCoinsValidator };
