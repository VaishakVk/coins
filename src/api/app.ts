import express, { Request, Response } from 'express';
import coinRoutes from './routes/coin';

const apiHandler = () => {
  const app = express();
  const port = process.env.PORT || 5001;

  app.use('/coin', coinRoutes);
  app.get('/', (_: Request, res: Response) => {
    return res.status(200).send('AoK');
  });

  app.listen(port, () => {
    console.log(`Application is running on port ${port}.`);
  });
};

export default apiHandler;
