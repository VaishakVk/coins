import { Router } from 'express';
import { getCoins } from '../../coin/coin.controller';
import { getCoinsValidator } from '../validator/coin';

const router = Router();

router.get('/', getCoinsValidator(), getCoins);

export default router;
