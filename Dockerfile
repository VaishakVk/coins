FROM node:12-alpine
WORKDIR /app
COPY package.json .
RUN npm install && npm install typescript -g
COPY . .
RUN tsc
EXPOSE 5001
CMD [ "node", "build/index.js" ]